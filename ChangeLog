2020-12-09  Bob Dubner <rdubner@symas.com>
	* Release 4.13
	* Made -save-temps processing delete .s files, per Simon's request
	* Made COBCD switch processing look like COBC's getopt() processing,
	  with sa, sav, save, save-, save-t, save-te, save-tem, save-temp, and
	  save-temps all being recognized with either one or two dashes.

2020-12-09  Bob Dubner <rdubner@symas.com>
	* Release 4.12
	* Repaired a naming problem where 4.11 was reporting 4.10
	* Updated (one might say repaired) the COBCD --help output

2020-12-09  Bob Dubner <rdubner@symas.com>
	* Release 4.10 -- depends on GnuCOBOL 3.1.1
	* Change COBCD so that when a .c file is not cleaned up, neither are the .h files

2020-12-09  Bob Dubner <rdubner@symas.com>
	* Release 4.9 -- depends on GnuCOBOL 3.1.1
	* Modify COBCD script so that -fsource-location is no longer used in COBC invocation

2020-11-30  Bob Dubner <rdubner@symas.com>
	* Release 4.8
	* COBCD behavior for -x, -m, -C, -S, -b, and -c matches COBC

2020-11-28  Bob Dubner <rdubner@symas.com>
	* eliminate need for -fsource-location by processing the new call cob_nop statements
	  when SFIX replaces and eliminates ./loc directives
	* Added capability for 'make install CPRINT=this COBCDFMODE=that'

2020-11-26  Bob Dubner <rdubner@symas.com>
	* Major rework to COBCD; it now handles all command lines in the 'make check' suite

2020-11-21  Bob Dubner <rdubner@symas.com>
	* Released Version 4.6
	* Changes for proper operation on Solaris/SPARC, handling the big-endian architecture
	  and 64-bit addresses that look like negative numbers.

2020-11-18  Bob Dubner <rdubner@symas.com>

	* Released Version 4.5
	* The COBCD processing tolerates the presence of symbolic names in size and offset
	  fields of the cob_dump_field() records.  (The Python doesn't handle them, yet.)
	* The VARIABLE_STRING_<filename> processing both in COBST and the Python GDB extension
	  has been modified to cope with filenames that contain characters that can't be
	  used in a C++ identifier.
	* The GnuCOBOL atlocal file can be modified to point to 'cobcd' instead of 'cobc'.
	  'make check' then runs successfully with only a few errors caused by the cobcd's use
	  of the -fsource-location switch.

2020-11-11  Bob Dubner <rdubner@symas.com>

	* First appearance of Version 4.5
	* Handles 'cobcd -x rtest.cbl say.o'
	* Eliminate traceback from 'cobcd --info'

2020-10-22  Bob Dubner <rdubner@symas.com>

	* Initial regression testing seems operable. See ../tests
	* Implemented p/x hexadecimal display
	* In p/d display, fixed Base: so that it actually displays base instead of field
	* Add ability for Python to change the frame
	* Change cobcd-st to use the list of .h files inside the .c file

2020-10-15  Bob Dubner <rdubner@symas.com>

	* Inaugurated this ChangeLog
	* First cut at a regression testing system, starting with test001 and test002


