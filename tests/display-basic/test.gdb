# testing basic cprint

source ../tests_init.gdb

file ./test

cstart
echo KNOWN GOOD START\n
echo TESTING cprint/v0 *\n
cprint/v0 *
echo TESTING cprint/v1 *\n
cprint/v1 *
echo TESTING cprint/v2 *\n
cprint/v2 *
echo TESTING cprint/v3 *\n
cprint/v3 *
echo TESTING cprint/v4 *\n
cprint/v4 *
echo TESTING cprint/v5 *\n
cprint/v5 *
echo TESTING cprint/v6 *\n
cprint/v6 *
echo TESTING cprint/v7 *\n
cprint/v7 *
echo TESTING cprint/v8 *\n
cprint/v8 *

echo \ncprint/v0 VAR-BINARY-DOUBLE-SIGNED-N\n
cprint/v0 VAR-BINARY-DOUBLE-SIGNED-N
echo \ncprint/v1 VAR-BINARY-DOUBLE-SIGNED-N\n
cprint/v1 VAR-BINARY-DOUBLE-SIGNED-N
echo \ncprint/v2 VAR-BINARY-DOUBLE-SIGNED-N\n
cprint/v2 VAR-BINARY-DOUBLE-SIGNED-N
echo \ncprint/v3 VAR-BINARY-DOUBLE-SIGNED-N\n
cprint/v3 VAR-BINARY-DOUBLE-SIGNED-N
echo \ncprint/v4 VAR-BINARY-DOUBLE-SIGNED-N\n
cprint/v4 VAR-BINARY-DOUBLE-SIGNED-N
echo \ncprint/v5 VAR-BINARY-DOUBLE-SIGNED-N\n
cprint/v5 VAR-BINARY-DOUBLE-SIGNED-N
echo \ncprint/v6 VAR-BINARY-DOUBLE-SIGNED-N\n
cprint/v6 VAR-BINARY-DOUBLE-SIGNED-N
echo \ncprint/v7 VAR-BINARY-DOUBLE-SIGNED-N\n
cprint/v7 VAR-BINARY-DOUBLE-SIGNED-N
echo \ncprint/v8 VAR-BINARY-DOUBLE-SIGNED-N\n
cprint/v8 VAR-BINARY-DOUBLE-SIGNED-N

echo KNOWN GOOD FINISH\n
continue
quit
