# testing ...

source ../tests_init.gdb

define printem
    echo KNOWN GOOD START\n
    echo cprint/v0 *\n
    cprint/v0 *
    echo cprint/v0 ?\n
    cprint/v0 ?
    echo cprint/v3 *\n
    cprint/v3 *
    echo cprint/v3 ?\n
    cprint/v3 ?
    echo cprint/x 1\n
    cprint/x 1
    echo cprint/m *\n
    cprint/m *
    echo cprint/m ?\n
    cprint/m ?
	echo cprint/x 1\n
	cprint/x 1
	echo cprint/b 1\n
	cprint/b 1
    echo KNOWN GOOD FINISH\n
end


file test
cprint/v1
cprint/r 6

break 73
echo KNOWN GOOD START\n
run
cprint disp
next
cprint disp-u
next
cprint dispp
next
cprint dispp-u
next
cprint disppp
next
cprint disppp-u
next
cprint bin
next
cprint bin-u
next
cprint cmp3
next
cprint cmp3-u
next
cprint cmp5
next
cprint cmp5-u
next
cprint cmp6
next
cprint cmpx
next
cprint cmpx-u
next
cprint cmpn
next
cprint cmpn-u
next
cprint chr
next
cprint chr-u
next
cprint shrt
next
cprint shrt-u
next
cprint long
next
cprint long-u
next
cprint dble
next
cprint dble-u
next
cprint dbl
next
cprint flt
next
#cprint r2d2
#next
cprint ppoint
next
cprint idx
next
cprint hnd
next
cprint alpnum
next
cprint alpha
next
cprint edit-num1
next
cprint edit-num2
next
cprint edit-num3
next
cprint edit-num4
next
cprint alphanumeric-data
next
echo KNOWN GOOD FINISH\n
next
#cprint nat
#next
#cprint net-num1
#next
#cprint net-num2
#next
#cprint net-num3
#next
#cprint net-num4
#next
cprint point
next
cprint numeric-data
next
cprint floating-data
next
cprint special-data
next
#cprint national-data
#next

#continue
delete
break 122
run
echo Filler for Redhat7/GDB7\n
echo KNOWN GOOD START\n
echo cprint dispp=.123456789\n
cprint dispp=.123456789
echo cprint disppp= 123456789\n
cprint disppp= 123456789
echo KNOWN GOOD FINISH\n


quit
